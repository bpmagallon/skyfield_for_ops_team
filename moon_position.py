from skyfield.api import load
import re

#download ephemris of moon and earth
de421 = load('de421.bsp')
moon  = de421['moon']
earth = de421['earth']

#set date
ts = load.timescale()
t = ts.utc(2018,10,15)

#eci moon position
mpos_ec = earth.at(t).observe(moon).position.km

stations_url = 'tle.txt'
dsatellites = load.tle(stations_url)
satellite = dsatellites['DIWATA-2']

#check TLE time
#epoch = satellite.epoch.utc_jpl().split(" UT")[0].split("A.D. ")[1].split(" ")
#epoch = re.sub(":", "_", epoch[0]+"_"+(epoch[1].split(".")[0]))
#epoch = re.sub("-", "_", epoch)

#compute for eci satellite position and velocity
satpos = satellite.at(t)

#satellite position in km
dpos_ec = satpos.position.km
velocity = satpos.velocity.km_per_s

