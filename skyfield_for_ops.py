#uses Skyfield library to produce and propagate ECI coordinates of Diwata-1 position
#uses Astropy to convert ECI coordinates to ECEF coordinates
from skyfield.api import Topos, load
from math import degrees, atan, sqrt, cos, sin
from skyfield.positionlib import ICRF
from astropy.time import Time
import datetime
import re

#format time for astropy input
def formatDate(date):
    date = str(date)
    date = date.split(" ")
    day = date[0].split("-")
    time = date[1].split(":")
    return int(day[0]),int(day[1]),int(day[2]),int(time[0]),int(time[1]),int(time[2][:2])

stations_url = 'tle.txt'
dsatellites = load.tle(stations_url)
satellite = dsatellites['DIWATA-1']  
epoch = satellite.epoch.utc_jpl().split(" UT")[0].split("A.D. ")[1].split(" ")
epoch = re.sub(":", "_", epoch[0]+"_"+(epoch[1].split(".")[0]))
epoch = re.sub("-", "_", epoch)

print epoch

#duration of the simulation, and step (in sec)
duration = 183
step = 1

#empty array, storage for satellite position from the propagation
locD = []
interval = 0
print "simulating"

while interval < duration:

    #date propagation
    #time step per seconds
    now = datetime.datetime(2017, 11, 13, 11, 55, 52, 0) + datetime.timedelta(0,interval) 
    interval+=step
    
    #reformate date for astropy
    inputDate =  formatDate(now)

    #set the time of the propagation
    ts = load.timescale()
    t = ts.utc(inputDate[0],inputDate[1],inputDate[2],inputDate[3],inputDate[4],inputDate[5])

    #get geocentric ECI position
    geocentric = satellite.at(t)

    #convert km to meters
    x1 = geocentric.position.km[0]*1000
    y1 = geocentric.position.km[1]*1000
    z1 = geocentric.position.km[2]*1000

    #get velocity
    v = geocentric.velocity.km_per_s
    
    now = Time(str(now))
    locD.append(["Time: "+str(now), "Position: x="+str(x1)+"km "+"y="+str(y1)+"km "+"z="+str(z1)+"km ", "Velocity: x="+str(v[0])+"km/s "+"y="+str(v[1])+"km/s "+"z="+str(v[2])+"km/s "])
